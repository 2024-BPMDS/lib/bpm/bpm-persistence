package de.ubt.ai4.petter.recpro.lib.bpmpersistence.tasklist;

import de.ubt.ai4.petter.recpro.lib.bpmpersistence.model.persistence.Tasklist;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
class TasklistPersistenceService {
    private TasklistRepository repository;

    public Tasklist saveAndFlush(Tasklist tasklist) {

        return repository.saveAndFlush(tasklist);
    }

    public Tasklist getLatestByAssigneeId(String assigneeId) {
        List<Tasklist> result = repository.getByAssigneeIdOrderByDateDesc(assigneeId);
        if (result.isEmpty()) {
            Tasklist res = new Tasklist();
            res.setAssigneeId(assigneeId);
            return res;
        } else {
            return result.get(0);
        }
    }

    public Tasklist getById(Long id) {
        return repository.findById(id).orElse(new Tasklist());
    }

    public Tasklist overwrite(Tasklist tasklist) {
        repository.delete(tasklist);
        return repository.saveAndFlush(tasklist);
    }
}
