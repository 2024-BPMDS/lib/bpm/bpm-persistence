package de.ubt.ai4.petter.recpro.lib.bpmpersistence.model.persistence;

import jakarta.persistence.Entity;
import jakarta.persistence.OneToMany;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;
import org.springframework.util.SerializationUtils;

import java.util.ArrayList;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter

@Entity
public class ProcessInstance extends BpmElementInstance {
    private String bpmsProcessInstanceId;

    @OneToMany
    @Cascade(CascadeType.ALL)
    private List<Task> tasks = new ArrayList<>();

    @Override
    public ProcessInstance copy() {
        ProcessInstance copy = SerializationUtils.clone(this);
        copy.setId(null);
        copy.setTasks(Task.copy(this.getTasks()));
        return copy;
    }
}
