package de.ubt.ai4.petter.recpro.lib.bpmpersistence;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages = {"de.ubt.ai4.petter.recpro", "de.ubt.ai4.petter.recpro.lib"})
public class BpmPersistenceApplication {

	public static void main(String[] args) {
		SpringApplication.run(BpmPersistenceApplication.class, args);
	}

}
