package de.ubt.ai4.petter.recpro.lib.bpmpersistence.task;

import de.ubt.ai4.petter.recpro.lib.bpmpersistence.model.persistence.Task;
import de.ubt.ai4.petter.recpro.lib.bpmpersistence.model.persistence.TaskState;
import de.ubt.ai4.petter.recpro.lib.bpmpersistence.processInstance.ProcessInstanceHelperService;
import de.ubt.ai4.petter.recpro.lib.bpms.execution.model.BpmsTask;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
@AllArgsConstructor
public class TaskHelperService {
    private TaskPersistenceService persistenceService;
    private ProcessInstanceHelperService processInstanceService;
    private Task fromBpmsTask(BpmsTask task) {
        Task databaseEntry = persistenceService.findByTaskId(task.getTaskId());
        Task result = new Task();
        result.setId(-1L);
        result.setTaskId(task.getTaskId());
        result.setPosition(-1);
        result.setAssigneeId(task.getAssignee());
        result.setActivityId(task.getActivityId());
        result.setProcessId(task.getProcessDefinitionId());
        result.setPriority(task.getPriority());
        result.setCreateDate(task.getCreateDate());
        result.setEditDate(databaseEntry.getEditDate());
        result.setDueDate(task.getDueDate());
        result.setExecutionId(task.getExecutionId());
        this.setState(result, task, databaseEntry);
        result.setRecproElementId(task.getActivityId());
        result.setRecproElementInstanceId(task.getTaskId());
        result = processInstanceService.createProcessInstance(task, result);
        return result;
    }

    public List<Task> fromBpmsTask(List<BpmsTask> tasks) {
        if (tasks.isEmpty()) {
            return new ArrayList<>();
        }
        return tasks.stream().map(this::fromBpmsTask).toList();
    }

    private void setState(Task result, BpmsTask task, Task databaseEntry) {
        if (databaseEntry.getState() != null) {
            result.setState(databaseEntry.getState());
        } else if (task.getAssignee() != null) {
            result.setState(TaskState.ASSIGNED);
        } else {
            result.setState(TaskState.OPEN);
        }
    }
}
