package de.ubt.ai4.petter.recpro.lib.bpmpersistence.processInstance;

import de.ubt.ai4.petter.recpro.lib.bpmpersistence.model.persistence.ProcessInstance;
import de.ubt.ai4.petter.recpro.lib.bpmpersistence.model.persistence.Task;
import de.ubt.ai4.petter.recpro.lib.bpmpersistence.task.TaskPersistenceService;
import de.ubt.ai4.petter.recpro.lib.bpms.execution.model.BpmsTask;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class ProcessInstanceHelperService {

    private ProcessInstanceService processInstanceService;
    private TaskPersistenceService taskPersistenceService;

    public Task createProcessInstance(BpmsTask task, Task recproTask) {
        ProcessInstance processInstance = processInstanceService.getByProcessInstanceId(task.getProcessInstanceId(), task.getProcessDefinitionId());
        recproTask.setProcessInstanceId(processInstance.getRecproElementInstanceId());
        recproTask = taskPersistenceService.saveAndFlush(recproTask);
        processInstance.getTasks().add(recproTask);
        processInstanceService.saveAndFlush(processInstance);
        return recproTask;
    }


}
