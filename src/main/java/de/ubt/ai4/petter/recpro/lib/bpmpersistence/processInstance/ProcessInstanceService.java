package de.ubt.ai4.petter.recpro.lib.bpmpersistence.processInstance;

import de.ubt.ai4.petter.recpro.lib.bpmpersistence.model.persistence.ProcessInstance;
import de.ubt.ai4.petter.recpro.lib.bpms.execution.service.process.BpmsExecutionProcessService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class ProcessInstanceService {
    private ProcessInstancePersistenceService persistenceService;
    private BpmsExecutionProcessService bpmsProcessService;

    public List<ProcessInstance> getAll() {
        return persistenceService.getAll();
    }

    public ProcessInstance getByProcessInstanceId(String processInstanceId, String processId) {
        return persistenceService.getByProcessInstanceId(processInstanceId, processId);
    }

    public ProcessInstance getById(Long id) {
        return persistenceService.getById(id);
    }

    public void startProcess(String processId) {
        this.bpmsProcessService.startProcess(processId);
    }

    public ProcessInstance saveAndFlush(ProcessInstance processInstance) {
        return this.persistenceService.saveAndFlush(processInstance);
    }
}
