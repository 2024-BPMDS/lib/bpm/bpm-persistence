package de.ubt.ai4.petter.recpro.lib.bpmpersistence.model.persistence;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Cascade;
import org.springframework.util.SerializationUtils;

import java.io.Serializable;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter

@Entity
public class Tasklist implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String assigneeId;
    @OneToMany
    @Cascade(org.hibernate.annotations.CascadeType.ALL)
    private List<Task> tasks = new ArrayList<>();
    private Instant date;

    public Tasklist copy() {
        Tasklist copy = SerializationUtils.clone(this);
        copy.setId(null);
        copy.setTasks(new ArrayList<>());
        this.getTasks().forEach(task -> copy.getTasks().add(task.copy()));
        return copy;
    }


    public List<Task> getTasksByProcessInstanceId(String id) {
        return this.getTasks().stream().filter(task -> task.getProcessInstanceId().equals(id)).toList();
    }

    public List<Task> getTasksByProcessId(String id) {
        return this.getTasks().stream().filter(task -> task.getProcessId().equals(id)).toList();
    }

    public List<Task> getTasksByActivityId(String id) {
        return this.getTasks().stream().filter(task -> task.getActivityId().equals(id)).toList();
    }

    public List<Task> getTasksByRecproElementId(String id) {
        return this.getTasks().stream().filter(task -> task.getRecproElementId().equals(id)).toList();
    }

    public List<Task> getTasksByRecproElementInstanceId(String id) {
        return this.getTasks().stream().filter(task -> task.getRecproElementInstanceId().equals(id)).toList();
    }


}
