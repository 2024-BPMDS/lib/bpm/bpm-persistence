package de.ubt.ai4.petter.recpro.lib.bpmpersistence.task;

import de.ubt.ai4.petter.recpro.lib.bpmpersistence.model.persistence.Task;
import de.ubt.ai4.petter.recpro.lib.bpmpersistence.model.persistence.TaskState;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor
public class TaskPersistenceService {
    private TaskRepository repository;

    public Task saveAndFlush(Task task) {
        return repository.saveAndFlush(task);
    }

    public Task findByTaskId(String taskId) {
        List<Task> result = this.repository.findTaskByTaskIdOrderByEditDateDesc(taskId);
        if (result.isEmpty()) {
            Task task = new Task();
            task.setState(TaskState.OPEN);
            return new Task();
        } else {
            return result.get(0);
        }
    }

    public Task findLastByUserId(String userId) {
        Optional<Task> result = this.repository.findTopByAssigneeIdAndStateOrderByEditDate( userId, TaskState.COMPLETED);
        return result.orElse(new Task());
    }
}
