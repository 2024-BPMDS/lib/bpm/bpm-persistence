package de.ubt.ai4.petter.recpro.lib.bpmpersistence.tasklist;

import de.ubt.ai4.petter.recpro.lib.bpmpersistence.model.persistence.Tasklist;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/recpro/bpm/execution/tasklist-lib/")
@AllArgsConstructor
public class TasklistController {
    private TasklistService service;

    @GetMapping("getAllTasks")
    public ResponseEntity<Tasklist> getAllTasks(@RequestHeader("X-User-ID") String assigneeId) {
        return ResponseEntity.ok(service.getTasklist(assigneeId));
    }
}
