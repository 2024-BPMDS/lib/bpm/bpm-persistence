package de.ubt.ai4.petter.recpro.lib.bpmpersistence.task;

import de.ubt.ai4.petter.recpro.lib.bpmpersistence.model.persistence.Task;
import de.ubt.ai4.petter.recpro.lib.bpmpersistence.model.persistence.TaskState;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
interface TaskRepository extends JpaRepository<Task, Long> {
    List<Task> findTaskByTaskIdOrderByEditDateDesc(String taskId);
    Optional<Task> findTopByAssigneeIdAndStateOrderByEditDate(String assigneeId, TaskState state);

}